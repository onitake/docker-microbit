FROM debian:buster

LABEL author "Gregor Riepl <onitake@gmail.com>"
LABEL description "Locally hosted version of github.com/Microsoft/pxt-microbit"
LABEL version "v1.1.61"

ARG pxt_version=3.22.22
ARG pxt_microbit_version=1.1.61

RUN apt-get -y update && apt-get -y upgrade

RUN apt-get -y install nodejs npm wget

ENV NPM_PACKAGES /pxt/.npm-packages

RUN \
	useradd -d /pxt -r -s /bin/false pxt && \
	mkdir -p /pxt && \
	mkdir -p ${NPM_PACKAGES} && \
	chown -R pxt:pxt /pxt
USER pxt
WORKDIR /pxt

RUN echo prefix=${NPM_PACKAGES} > ${HOME}/.npmrc

# use this:
RUN \
	wget -O pxt-${pxt_version}.tar.gz https://github.com/Microsoft/pxt/archive/v${pxt_version}.tar.gz && \
	tar xvzf pxt-${pxt_version}.tar.gz
RUN \
	wget -O pxt-microbit-${pxt_microbit_version}.tar.gz https://github.com/Microsoft/pxt-microbit/archive/v${pxt_microbit_version}.tar.gz && \
	tar xvzf pxt-microbit-${pxt_microbit_version}.tar.gz
# or:
#RUN git clone https://github.com/microsoft/pxt
#RUN git clone https://github.com/microsoft/pxt-microbit --branch v1

RUN \
	cd pxt-${pxt_version} && \
	npm install && \
	npm run build

RUN \
	cd pxt-microbit-${pxt_microbit_version} && \
	npm install -g pxt && \
	npm install && \
	npm link ../pxt-${pxt_version}

EXPOSE 3232
EXPOSE 3233
WORKDIR /pxt/pxt-microbit-${pxt_microbit_version}
CMD ${NPM_PACKAGES}/bin/pxt serve --hostname 0.0.0.0 --no-browser --port 3232 --wsport 3233 --no-serial --cloud
